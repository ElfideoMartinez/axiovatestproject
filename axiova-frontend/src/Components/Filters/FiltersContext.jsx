import React from 'react'

const FilterContext = React.createContext({
  filters: [
    { name: 'price', value: 'All' },
    { name: 'comboPrice', value: 'All' },
    { name: 'Topping', value: 'All' },
    { name: 'size', value: 'All' },
    {name:'sortBy', value:'All'}
  ],
  setFilterValue: () => {}
})

export default FilterContext