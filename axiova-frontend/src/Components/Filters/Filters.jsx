/* eslint-disable react/prop-types */
import { useContext, useState } from 'react';
import Styles from '../SearchComponent/SearchComponent.module.css';
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import { faAngleDown } from '@fortawesome/free-solid-svg-icons';
import FilterContext from './FiltersContext';

export const Filters = ({ options, icon }) => {
  const [isOpen, setIsOpen] = useState(false);
  const [selectedOption, setSelectedOption] = useState(options.label);
  const { setFilterValue } = useContext(FilterContext)

  const toggleDropdown = () => {
    setIsOpen(!isOpen);
  };

  const handleOptionClick = option => {
    setSelectedOption(option.label);
    setFilterValue(options.name, option.value)
    setIsOpen(false);
  };

  return (
    <div>
      <div className={Styles.mainContainer} onClick={toggleDropdown}>
        <FontAwesomeIcon className={Styles.searchIcon} icon={icon} />
        <p>{selectedOption}</p>
        <FontAwesomeIcon icon={faAngleDown} className={Styles.searchIcon} />
      </div>
      {(isOpen && options.options) && (
        <div className={Styles.dropDownMenu}>
          {options.options.map(option => (
            <div
              className={Styles.dropDownMenuItem}
              key={option.value}
              onClick={() => handleOptionClick(option)}>
              {option.label}
            </div>
          ))}
        </div>
      )}
    </div>
  );
};
