/* eslint-disable react/prop-types */
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome';
import Styles from './SearchComponent.module.css';
import { faSearch } from '@fortawesome/free-solid-svg-icons';

const SearchComponent = ({ handleSearchProp }) => {
  const handleSearch = (e) => {
    handleSearchProp(e.target.value)
  };
  const handlekeyDown = e => {
    if (e.key === 'Enter') {
      console.log('Search');
    }
  };

  return (
    <div className={Styles.mainContainer}>
      <FontAwesomeIcon
        onClick={handleSearch}
        className={Styles.searchIcon}
        width="15px"
        height="15px"
        color="#9b9b9b"
        icon={faSearch}
      />
      <input
        onChange={e => { handleSearch (e)}}
        onKeyDown={handlekeyDown} type="text" placeholder="NOMBRE" />
    </div>
  );
};

export default SearchComponent;
