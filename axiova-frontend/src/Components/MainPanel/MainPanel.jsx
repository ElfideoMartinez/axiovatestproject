import { useEffect, useState } from 'react';
import ResultsPanel from '../ResultsPanel/ResultsPanel';
import SearchPanel from '../SearchPanel/SearchPanel';
import styles from './MainPanel.module.css';
import FilterContext from '../Filters/FiltersContext';

const MainPanel = () => {
  const [data, setData] = useState([]);
  const [searchQuery, setSearchQuery] = useState('');
  const [filters, setFilters] =useState([
    { name: 'price', value: 'All' },
    { name: 'comboPrice', value: 'All' },
    { name: 'Topping', value: 'All' },
    { name: 'size', value: 'All' },
    { name: 'sortBy', value: 'All' },
  ]);

  const fetchData = async () => {
    const response = await fetch('http://localhost:1337/api/cakes');
    const data = await response.json();
    setData(data.data);
  };

  useEffect(() => {
    fetchData();
  }, []);

  const handleSearch = query => {
    setSearchQuery(query);
  };
  //make function to handle filter change, change the value of the filter
  const handleFilterChange = (name, value) => {
    const newFilters = filters.map(filter => {
      if (filter.name === name) {
        return { ...filter, value };
      }
      return filter;
    });
    setFilters(newFilters);
  };

  const filteredData = data.filter(cake => {
    let match = cake.attributes.name.toLowerCase().includes(searchQuery.toLowerCase());

    filters.forEach(filter => {
      if (filter.value !== 'All') {
        switch (filter.name) {
          case 'price':
            match = match && cake.attributes.price < Number(filter.value);
            break;
          case 'comboPrice':
            match = match && cake.attributes.comboPrice < Number(filter.value);
            break;
          case 'Topping':
            match = match && cake.attributes.toppingType === filter.value;
            break;
          case 'size':
            match = match && cake.attributes.size === filter.value;
            break;
          default:
            break;
        }
      }
    });
    return match;
  }).sort((a, b) => { 
    if (filters[4].value === 'All') {
      return 0;
    }
    if (filters[4].value === 'price') {
      return a.attributes.price - b.attributes.price;
    }
    if (filters[4].value === 'comboPrice') {
      return a.attributes.comboPrice - b.attributes.comboPrice;
    }
    if (filters[4].value === 'topping') {
      return a.attributes.toppingType.localeCompare(b.attributes.toppingType);
    }
    if (filters[4].value === 'name') {
      return a.attributes.name.localeCompare(b.attributes.name);
    }
    return 0;
  });

  return (
    <div className={styles.mainContainer}>
      <FilterContext.Provider value={
        {
          filters,
          setFilterValue: handleFilterChange,
        }
      }>
      <SearchPanel onSearch={handleSearch} />
        <ResultsPanel cakeList={filteredData} />
      </FilterContext.Provider>
    </div>
  );
};

export default MainPanel;
