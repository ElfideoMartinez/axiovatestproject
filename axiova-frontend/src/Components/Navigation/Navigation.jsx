import styles from './Navigation.module.css';
import logo from '../../assets/MiPasteleria.svg';

export const Navigation = () => {
  return (
    <div className={styles.mainContainer}>
      <img src={logo} alt="Mi Pastelería Logo" />
      <div>
        <p>Pasteles</p>
        <p>Helados</p>
        <p>Galletas</p>
        <p>Nosotros</p>
        <p>Sucursales</p>
      </div>
    </div>
  );
};
