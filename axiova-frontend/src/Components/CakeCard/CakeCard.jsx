/* eslint-disable react/prop-types */
import Styles from './CakeCard.module.css';

export const CakeCard = ({ cake }) => {

  const cakeVariants = {
    size: {
      S: 'Chico,',
      M: 'Mediano',
      L: 'Grande',
    },
    sizing: {
      L: '15-17 Personas',
      M: '8 Personas',
      S: '2 Personas',
    },
    toppingType: {
      0: 'Fondeau',
      1: 'Betun Italiano',
      2: 'Chantilly',
    },
  };
  //capitalize the first letter of the cake name
  const cakeName = cake.attributes.name.charAt(0).toUpperCase() + cake.attributes.name.slice(1);
  const cakeSize = cakeVariants.size[cake.attributes.size];
  const cakeTopping = cakeVariants.toppingType[cake.attributes.toppingType];
  const cakeSizing = cakeVariants.sizing[cake.attributes.size];
  //remove the last two digits from the price is too big to be displayed
  const price = parseFloat(cake.attributes.price).toFixed(2);
  const comboPrice = parseFloat(cake.attributes.comboPrice).toFixed(2);

  return (
    <div className={Styles.mainContainer}>
      <div className={Styles.header}>
        <h2 style={{ color: '#434244' }}>{cakeName}</h2>
        {/*combo presentation Start*/}
        <div
          style={{
            display: 'flex',
            flexDirection: 'row',
            gap: '2px',
            height: '100%',
          }}>
          <div
            className={Styles.comboSelector}
            style={{
              backgroundColor: '#eb5e70',
            }}>
            <p>PASTEL</p>
          </div>
          <div
            className={Styles.comboSelector}
            style={{ backgroundColor: '#f7aa76' }}>
            <p>PAQUETE</p>
          </div>
        </div>
        {/*combo presentation End*/}
      </div>
      {/*Start Content div*/}
      <div className={Styles.content}>
        <div className={Styles.imageContainer}>
          <img src={cake.attributes.imageUrl} alt="cake" />
        </div>
        <div className={Styles.descriptionContainer}>
          <p>{cake.attributes.description}</p>
          <p style={{ margin: 0 }}>
            <span style={{ fontWeight: 'bold' }}>Tamaño: </span>
            {cakeSize}
          </p>
          <p style={{ margin: 0 }}>
            <span style={{ fontWeight: 'bold' }}>Toping: </span>
            {cakeTopping}
          </p>
        </div>
        {/*Start Price div*/}
        <div
          id="priceContainer"
          style={{
            display: 'flex',
            flexDirection: 'row',
            gap: '2px',
            height: '100%',
          }}>
          <div className={Styles.selectionContainer}>
            <h5>{cakeSizing}</h5>
            <h1>${price}</h1>
            <h5 style={{ color: '#808285', margin: 0, marginBottom: '10px' }}>
              *Solo Pastel
            </h5>
            <button className={Styles.selectButton}>SELECCIONAR</button>
          </div>
          <div className={Styles.selectionContainer}>
            <h5>Combo Fiesta</h5>
            <h1>${comboPrice}</h1>
            <h5 style={{ color: '#808285', margin: 0, marginBottom: '10px' }}>
              *Solo Pastel
            </h5>
            <button className={Styles.selectButton2}>SELECCIONAR</button>
          </div>
        </div>
      </div>
    </div>
  );
};
