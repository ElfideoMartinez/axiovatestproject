/* eslint-disable react/prop-types */
import {
  faArrowUpWideShort,
  faCakeCandles,
  faMoneyBill,
  faRulerCombined,
} from '@fortawesome/free-solid-svg-icons';
import { Filters } from '../Filters/Filters';
import SearchComponent from '../SearchComponent/SearchComponent';
import Styles from './SearchPanel.module.css';

const SearchPanel = ({ onSearch }) => {

  const options1 = {
    options: [
      { value: 'All', label: 'Todos' },
      { value: 401, label: '<$401' },
      { value: 601, label: '<$601' },
    ],
    label: 'Precio',
    name: 'price'
  };
  const options2 = {
    options: [
     { value: 'All', label: 'Todos' },
      { value: 101, label: '<500' },
      { value: 501, label: '<= 500' },
      { value: 1501, label: '>500' },
    ],
    label: 'Precio Combo',
    name: 'comboPrice'
    
  };
  const options3 = {
    options: [
     { value: 'All', label: 'Todos' },
      { value: '0', label: 'Fondeau' },
      { value: '1', label: 'Betun Italiano' },
      { value: '2', label: 'Chantilly' },
    ],
    label: 'Topping',
    name: 'Topping',
  };
  const options4 = {
    options: [
     { value: 'All', label: 'Todos' },
      { value: 'S', label: 'Pequeño' },
      { value: 'M', label: 'Mediano' },
      { value: 'L', label: 'Grande' },
    ],
    label: 'Tamaño',
    name: 'size'
  };
  const options5 = {
    options: [
     { value: 'All', label: 'Todos' },
      { value: 'name', label: 'Nombre' },
      { value: 'comboPrice', label: 'Precio Combo' },
      { value: 'topping', label: 'Topping' },
      { value: 'size', label: 'Tamaño' },
    ],
    label: 'Ordenar por',
    name: 'sortBy'
  };
  return (
    <div className={Styles.mainContainer}>
      <SearchComponent handleSearchProp={onSearch} />
      <Filters options={options1} icon={faMoneyBill} />
      <Filters options={options2} icon={faMoneyBill} />
      <Filters options={options3} icon={faCakeCandles} />
      <Filters options={options4} icon={faRulerCombined} />
      <Filters options={options5} icon={faArrowUpWideShort} />
    </div>
  );
};

export default SearchPanel;
