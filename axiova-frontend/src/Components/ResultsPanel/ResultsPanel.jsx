/* eslint-disable react/prop-types */
import { CakeCard } from '../CakeCard/CakeCard';

const ResultsPanel = ({ cakeList }) => {
  return (
    <div
      style={{
        display: 'flex',
        alignItems: 'center',
        flexDirection: 'column',
        gap: '15px',
      }}>
      {cakeList.map(cake => {
        return <CakeCard cake={cake} key={cake.id} />;
      })}
    </div>
  );
};

export default ResultsPanel;
