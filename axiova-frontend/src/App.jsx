import ContactInfo from './Components/ContactInfo/ContactInfo.moduel';
import { Navigation } from './Components/Navigation/Navigation';
import MainPanel from './Components/MainPanel/MainPanel';

function App() {
  return (
    <div>
      <ContactInfo />
      <Navigation />
      <MainPanel />
    </div>
  );
}

export default App;
