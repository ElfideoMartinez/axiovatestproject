# Evaluación de Axiova



# Getting started

## Prerequisites
- Node.js

- Package manager (e.g. Yarn or NPM)

you'll need to have this two installed in order to run the app

## Installation
1) Clone the repo

```
git clone https://github.com/ElfideoMartinez/axiovatestproject.git
```

## Install dependencies for frontend

```
cd axiova-frontend
yarn install
```
## Start the frontend server

```
yarn dev
```
## Install dependencies for backend

```
cd ../axiova-backend
yarn install
```
## Start the backend server

```
yarn develop
```
## note
Note that the URL for the host where the backend server is running may change, and you will need to update the API calls in the frontend accordingly.
